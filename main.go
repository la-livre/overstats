package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/fatih/color"
)

var debug bool

func main() {
	isBot := flag.Bool("bot", false, "do we want to run it as a client?")
	flag.BoolVar(&debug, "debug", false, "For debug purpose")
	flag.Parse()
	if !*isBot {
		userTag := askFor("what is the usertag you want to look for?", "user")
		usr, err := getUser(userTag, "", "", false)
		if err != nil {
			color.Red(err.Error() + "\nPress enter to exit the program")
			io := bufio.NewReader(os.Stdin)
			io.ReadString('\n')
		}
		fmt.Printf("%+v\n", usr)
		println("The program will now try to save your stats in more easily readable format")
		byteUsr, err := json.MarshalIndent(usr, "", "\t")
		if err != nil {
			color.Red("It seems like the program couldn't understand the user. Maybe the response playoverwatch.com sent was incoplete or corrupted\n")
			color.Red(err.Error() + "\nPress enter to exit the program")
			io := bufio.NewReader(os.Stdin)
			io.ReadString('\n')
		}
		if err := ioutil.WriteFile("user.json", byteUsr, 0644); err != nil {
			color.Red("It seems like the program couldn't save the user. Maybe you haven't got the permissions to write files. Please contact your system's admin for further details\n")
			color.Red(err.Error() + "\nPress enter to exit the program")
			io := bufio.NewReader(os.Stdin)
			io.ReadString('\n')
		}
		color.Green("\n-Looks like everythng went fine! Now you can read your stats in the user.json file!")
		color.Green("\nPress enter to exit the program")
		io := bufio.NewReader(os.Stdin)
		io.ReadString('\n')
	}
	//panic("Any other use is not ready")
	tokenRaw, err := ioutil.ReadFile("./auth.json")
	if err != nil {
		println("Did you even provide an auth.json file ?")
		color.Red(err.Error() + "\nPress enter to exit the program")
		io := bufio.NewReader(os.Stdin)
		io.ReadString('\n')
	}
	var authBot auth
	json.Unmarshal(tokenRaw, &authBot)
	dg, err := discordgo.New("Bot " + authBot.Authtoken)
	if err != nil {
		fmt.Println("Error creating Discord session: ", err)
		return
	}
	dg.AddHandler(onNewMessage)
	dg.AddHandler(onReady)
	if err := dg.Open(); err != nil {
		fmt.Println("Error opening Discord session: ", err)
	}
	fmt.Println("Running.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

func askFor(text string, subject string) string {
	print(text + " ")
	io := bufio.NewReader(os.Stdin)
	userTag, err := io.ReadString('\n')
	if err != nil {
		panic(err)
	}
	if len([]byte(userTag)) >= 2 {
		str := string([]byte(userTag)[:len([]byte(userTag))-2])
		print(str)
		if subject == "platform" && !(str == "pc" || str == "xbl" || str == "psn" || str == "") {
			color.Red("\nInvalid platform, falling back to default (pc)")
			return "pc"
		} else if subject == "platform" {
			if str == "" {
				color.Yellow("\nNo plaftorm selected, falling back to default (pc) ")
				return "pc"
			}
			return str
		}
		if subject == "region" && !(str == "eu" || str == "us" || str == "kr" || str == "") {
			color.Red("\nInvalid region, falling back to default (eu)")
			return "eu"
		} else if subject == "region" {
			if str == "" {
				color.Yellow("\nNo region selected, falling back to default (eu) ")
				return "eu"
			}
			return str
		}
		if subject == "user" && str == "" {
			color.Red("\nNo tag provided. I'm just gonna crash myself because I don't want to deal with your bullshit, just learn to read\n")
			time.Sleep(time.Duration(5) * time.Second)
			panic("I told you I would do it.")
		} else if subject == "user" {
			return str
		}
	}
	return "\n"
}

func askInfos() (string, string) {
	platform := askFor("\nWhat platform are you playing on? (pc, xbl(xbox), psn(playstation)) (default: pc)", "platform")
	region := askFor("\nWhat region do you want to search in? (us, eu, kr(asia)) (default: eu)", "region")
	return platform, region
}

func checkResponse(res *http.Response) error {
	if res.StatusCode != 200 {
		if res.StatusCode == 404 {
			return errors.New("Error: " + strconv.Itoa(res.StatusCode) + ". No user found using provided platform, region and Battletag. Make sure you did not provide wrong infos")
		} else if res.StatusCode == 500 {
			return errors.New("Error: " + strconv.Itoa(res.StatusCode) + ". My good sir, it seems the overwatch website is having internal issues which are certainly NOT my fault. This is an  OW-side error")
		} else {
			return errors.New("Error: " + strconv.Itoa(res.StatusCode) + ". I have no idea man, I just implemented the 404 and 500 error, so this one I have like nooooooooo idea. I mean, i may have done if it was really me, but instead of da sexy geek gril, it's a piece of software which is talking to you, so what did you expect? Sentience?")
		}
	}
	return nil

}

func reverse(numbers []int) []int {
	for i := 0; i < len(numbers)/2; i++ {
		j := len(numbers) - i - 1
		numbers[i], numbers[j] = numbers[j], numbers[i]
	}
	return numbers
}

func parseDuration(durationRaw string) (time.Duration, error) {
	var duration time.Duration
	var unitList []int
	listTimeRaw := strings.Split(durationRaw, ":")
	for _, listTime := range listTimeRaw {
		unit, err := strconv.Atoi(listTime)
		if err != nil {
			return duration, err
		}
		unitList = append(unitList, unit)
	}
	unitList = reverse(unitList)
	if len(unitList) >= 1 {
		duration = duration + time.Duration(unitList[0])*time.Second
		if len(unitList) >= 2 {
			duration = duration + time.Duration(unitList[1])*time.Minute
			if len(unitList) >= 3 {
				duration = duration + time.Duration(unitList[2])*time.Hour
			}
		}
	}
	return duration, nil
}
