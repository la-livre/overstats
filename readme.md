# Overstat

A little go program (AGAIN)

### Getting started

Link to binaries may be added later on, once the project is ready *LOOK IN THE BIN FOLDER*

* To compile this, you need to have the lastest [golang compiler](https://golang.org/doc/install) installed and configured on your system
* Once it is done, type down ```go get``` or ```go get``` in your terminal once you're in the project's directory
* You can now ```go build``` which will compile the whole thing

### How to use

You can launch is right away by double clicking it, or launch it *via* the cmd/terminal, just follow the instructions

#### If you don't use this as a bot, and launch it without the bot flag (see below), this is more like a stat saver which will save your stats to user.json (or something)


Also, if you want to launch it as a bot, you have to launch it in the terminal with the -bot flag, like `./overstats.exe -bot`

Go to [the Discord dev site](https://discordapp.com/developers/applications/me) (I'm assuming you're logged in) create a new app, make it a bot user, not necessarly a PUBLIC BOT if you want to be the only one who can add the bot to a server, which I actually recommend, because too much people using your bot may result in a suspectfully high bandwidth usage, and Blizzard *MIGHT* not like it.

Then, in the app bot user pannel click on the link to reveal your bot's token, and paste it in a auth.json in the same directory as the binary. It has to look like
```
{
    "authtoken": "XOXOXOXOXOXOXOXOXOXOXOXOXOXOXO"
}
```
Except I highly doubt your token will actually be "XOXOXOXOXOXOXOXOXOXOXOXOXOXOXO". (this program will not send to me or any third party your credentials/whatever. This just does a web request, and the bot is hosted on your computer/server entirely thanks to [Discord-go](https://github.com/bwmarrin/discordgo))

You can now launch the binary in the terminal as I said before, and enjoy!

As the caching logic is not implemented yet, please go easy on the bot. I'm planning to do that soon, but as school started over again, most of my programming time is in economics classes sooooooooo I promise I will do it

One day

### Wanna test it?

Don't hesitate to contact me if you have trouble using the bot!

### How to contribute

It would be very appreciated to take contact with [me](mailto:cleo.rebert@gmail.com) so we can somewhat work on different things instead of doing the same job twice, which would be kinda pointless. Working with someone could be a nice experience for me! o/

## Dislaimer

While the code does works surprisingly well for a program of mine, the source code is pretty ugly in some cases, I'll probably rewrite important parts of it later so it's prettier, please also note that I did implement it as a bot, as a stat retriever, but the REST api part is gonna be very nice, or at least I hope so, once I have implemented the cache logic, I think everyone will be able to use it without troubles (Since this whole thing is kinda hacky, having to download the playoverwatch page is a real pain (really, this is pretty impressive for just one page))