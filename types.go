package main

import (
	"time"
)

type auth struct {
	Authtoken string `json:"authtoken"`
}

type user struct {
	Quickplay          gamemodeStats
	Competitive        gamemodeStats
	LastChecked        time.Time
	Username           string
	Tag                string
	Level              int
	CompetitiveRank    int
	TotalGamesWon      int
	TotalObjectiveTime time.Duration
	TotalDeaths        int
	TotalKills         int
}

type gamemodeStats struct { // HOLY SHIT IT'S DONE
	Combat        combat        // Done
	Assist        assist        // Done
	Best          best          // Done
	Death         death         // Done
	Awards        awards        // Done
	Average       average       // Done
	Game          game          // Done
	Miscellaneous miscellaneous // Done
}

type combat struct {
	MeleeFinalBlows     int
	TotalFinalBlows     int // MeleeFinalBlows are part of TotalFinalBlows
	SoloKills           int
	Multikills          int
	EnvirronmentalKills int
	ObjectiveKills      int
	TotalKills          int // All the kills described above are part of TotalKills
	TotalDamageDone     int64
}

type assist struct {
	HealingDone       int64
	ReconAssist       int // People you revealed with an ability and that another player killed
	TelepadsDestroyed int
}

type best struct {
	TotalKills       int
	FinalBlows       int
	DamageDone       int
	HealingDone      int
	DefensiveAssists int // When you heal someone and they kill someone
	OffensiveAssists int
	ObjectiveKills   int
	ObjectiveTime    time.Duration // The time pushing the wagon/on the flag/whatever that game does
	Multikill        int
	SoloKills        int
	TimeSpentOnFire  time.Duration
}

type death struct {
	EnvirronmentalDeaths int
	TotalDeaths          int
}

type awards struct {
	Cards        int
	BronzeMedals int
	SilverMedals int
	GoldMedals   int
	TotalMedals  int
}

type average struct {
	MeleeFinalBlows    float64
	TotalFinalBlows    float64
	TimeSpentOnFire    time.Duration
	ObjectiveTime      time.Duration
	SoloKills          float64
	ObjectiveKills     float64
	TotalKills         float64
	HealingDone        int
	Deaths             float64
	DamageInTenMinutes int
}

type game struct {
	GamesWon        int
	GamesPlayed     int
	TimeSpentOnFire time.Duration
	ObjectiveTime   time.Duration
	HoursPlayed     int
}

type miscellaneous struct {
	BestKillStreak                 int
	MostTurretsDestroyed           int // In a single game
	MostTelepadsDestroyed          int
	MostShieldGeneratorsDestroyed  int
	MostReconAssist                int
	MostEnvironnementalKills       int
	MostMeleeFinalBlows            int
	TotalTurretsDestroyed          int
	TotalShieldGeneratorsDestroyed int
	TotalOffensiveAssists          int
	TotalDefensiveAssists          int
}
