package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/html"

	"github.com/PuerkitoBio/goquery"
)

func getUserFromDB(tag string) (user, error) {
	cacheFile, err := ioutil.ReadFile("./cache.json")
	if err != nil {
		panic(err)
	}
	var userList []user
	if err := json.Unmarshal(cacheFile, &userList); err != nil {
		return user{}, errors.New("Cache corrupted")
	}
	for _, usr := range userList {
		if usr.Tag == tag {
			if time.Since(usr.LastChecked) <= time.Duration(5)*time.Minute {
				return usr, nil
			}
		}
		return user{}, errors.New("Cache expired")
	}
	return user{}, errors.New("No matching user")
}

func getUser(tag string, platformRaw string, regionRaw string, isBot bool) (user, error) {
	// usr, err := getUserFromDB(tag)
	// if err == nil {
	//	return usr, nil
	//}
	var usr user
	var platform string
	var region string
	if !isBot {
		platform, region = askInfos()
	} else {
		platform, region = platformRaw, regionRaw
	}
	// println("https://playoverwatch.com/en-us/career/" + platform + "/" + region + "/" + tag)
	res, _ := http.Get("https://playoverwatch.com/en-us/career/" + platform + "/" + region + "/" + tag)
	if err := checkResponse(res); err != nil {
		return user{}, err
	}
	doc, err := goquery.NewDocumentFromResponse(res)
	if err != nil {
		log.Fatal(err)
	}
	usr.Username = doc.Find("h1.header-masthead").Text()
	usr.LastChecked = time.Now()
	lvl, err := strconv.Atoi(doc.Find("div.masthead-player-progression.show-for-lg>div>div.u-vertical-center").Text())
	if err != nil {
		lvl = 0
	}
	rank, err := strconv.Atoi(doc.Find("div.masthead-player-progression.show-for-lg>div.competitive-rank>div.u-align-center").Text())
	if err != nil {
		rank = 0
	}
	usr.Level = lvl
	usr.CompetitiveRank = rank
	baseQuickplay := doc.Find("#quickplay>section.career-stats-section>div>div.is-active>div>div>table").Nodes
	baseCompetitive := doc.Find("#competitive>section.career-stats-section>div>div.is-active>div>div>table").Nodes
	if debug {
		fmt.Println("Quick: " + strconv.Itoa(len(baseQuickplay)))
		fmt.Println("Comp: " + strconv.Itoa(len(baseCompetitive)))
	}
	for _, node := range baseQuickplay {
		card := goquery.NewDocumentFromNode(node)
		typeOfCard := card.Find("thead>tr>th>h5").Text()
		if debug {
			println("Type: " + typeOfCard)
		}
		var err error
		switch typeOfCard {
		case "Combat":
			err = usr.Quickplay.Combat.Populate(card.Find("tbody>tr").Nodes)
		case "Assists":
			err = usr.Quickplay.Assist.Populate(card.Find("tbody>tr").Nodes)
		case "Best":
			err = usr.Quickplay.Best.Populate(card.Find("tbody>tr").Nodes)
		case "Deaths":
			err = usr.Quickplay.Death.Populate(card.Find("tbody>tr").Nodes)
		case "Average":
			err = usr.Quickplay.Average.Populate(card.Find("tbody>tr").Nodes)
		case "Match Awards":
			err = usr.Quickplay.Awards.Populate(card.Find("tbody>tr").Nodes)
		case "Game":
			err = usr.Quickplay.Game.Populate(card.Find("tbody>tr").Nodes)
		case "Miscellaneous":
			err = usr.Quickplay.Miscellaneous.Populate(card.Find("tbody>tr").Nodes)
		default:
		}
		if err != nil {
			return usr, err
		}
	}
	for _, node := range baseCompetitive {
		card := goquery.NewDocumentFromNode(node)
		typeOfCard := card.Find("thead>tr>th>h5").Text()
		if debug {
			println("Type: " + typeOfCard)
		}
		var err error
		switch typeOfCard {
		case "Combat":
			err = usr.Competitive.Combat.Populate(card.Find("tbody>tr").Nodes)
		case "Assists":
			err = usr.Competitive.Assist.Populate(card.Find("tbody>tr").Nodes)
		case "Best":
			err = usr.Competitive.Best.Populate(card.Find("tbody>tr").Nodes)
		case "Deaths":
			err = usr.Competitive.Death.Populate(card.Find("tbody>tr").Nodes)
		case "Average":
			err = usr.Competitive.Average.Populate(card.Find("tbody>tr").Nodes)
		case "Match Awards":
			err = usr.Competitive.Awards.Populate(card.Find("tbody>tr").Nodes)
		case "Game":
			err = usr.Competitive.Game.Populate(card.Find("tbody>tr").Nodes)
		case "Miscellaneous":
			err = usr.Competitive.Miscellaneous.Populate(card.Find("tbody>tr").Nodes)
		default:
		}
		if err != nil {
			return usr, err
		}
	}
	usr.TotalDeaths = usr.Competitive.Death.TotalDeaths + usr.Quickplay.Death.TotalDeaths
	usr.TotalGamesWon = usr.Competitive.Game.GamesWon + usr.Quickplay.Game.GamesWon
	usr.TotalKills = usr.Competitive.Combat.TotalKills + usr.Quickplay.Combat.TotalKills
	usr.TotalObjectiveTime = usr.Competitive.Game.ObjectiveTime + usr.Quickplay.Game.ObjectiveTime
	usr.Tag = tag
	return usr, nil
}

func (usr *combat) Populate(lines []*html.Node) error {
	for _, lineRaw := range lines {
		line := goquery.NewDocumentFromNode(lineRaw)
		text := line.Find("td").First().Text()
		value := strings.Replace(line.Find("td").First().Next().Text(), ",", "", -1)
		if debug {
			println(text + ": " + value)
		}
		var err error
		switch text {
		case "Melee Final Blows":
			usr.MeleeFinalBlows, err = strconv.Atoi(value)
		case "Final Blows":
			usr.TotalFinalBlows, err = strconv.Atoi(value)
		case "Solo Kills":
			usr.SoloKills, err = strconv.Atoi(value)
		case "Multikills":
			usr.Multikills, err = strconv.Atoi(value)
		case "Environmental Kills":
			usr.EnvirronmentalKills, err = strconv.Atoi(value)
		case "Objective Kills":
			usr.ObjectiveKills, err = strconv.Atoi(value)
		case "Eliminations":
			usr.TotalKills, err = strconv.Atoi(value)
		case "All Damage Done":
			usr.TotalDamageDone, err = strconv.ParseInt(value, 10, 64)
		default:

		}
		if err != nil {
			println("Error occured")
			return err
		}
	}
	return nil
}

func (usr *assist) Populate(lines []*html.Node) error {
	for _, lineRaw := range lines {
		line := goquery.NewDocumentFromNode(lineRaw)
		text := line.Find("td").First().Text()
		value := strings.Replace(line.Find("td").First().Next().Text(), ",", "", -1)
		var err error
		switch text {
		case "Recon Assists":
			usr.ReconAssist, err = strconv.Atoi(value)
		case "Teleporter Pads Destroyed":
			usr.TelepadsDestroyed, err = strconv.Atoi(value)
		case "Healing Done":
			usr.HealingDone, err = strconv.ParseInt(value, 10, 64)
		default:

		}
		if err != nil {
			println("Error occured")
			return err
		}
	}
	return nil
}

func (usr *best) Populate(lines []*html.Node) error {
	for _, lineRaw := range lines {
		line := goquery.NewDocumentFromNode(lineRaw)
		text := line.Find("td").First().Text()
		value := strings.Replace(line.Find("td").First().Next().Text(), ",", "", -1)
		var err error
		switch text {
		case "Eliminations - Most in Game":
			usr.TotalKills, err = strconv.Atoi(value)
		case "Final Blows - Most in Game":
			usr.FinalBlows, err = strconv.Atoi(value)
		case "All Damage Done - Most in Game":
			usr.DamageDone, err = strconv.Atoi(value)
		case "Healing Done - Most in Game":
			usr.HealingDone, err = strconv.Atoi(value)
		case "Defensive Assists - Most in Game":
			usr.DefensiveAssists, err = strconv.Atoi(value)
		case "Offensive Assists - Most in Game":
			usr.OffensiveAssists, err = strconv.Atoi(value)
		case "Objective Kills - Most in Game":
			usr.ObjectiveKills, err = strconv.Atoi(value)
		case "Objective Time - Most in Game":
			usr.ObjectiveTime, err = parseDuration(value)
		case "Multikill - Best":
			usr.Multikill, err = strconv.Atoi(value)
		case "Solo Kills - Most in Game":
			usr.SoloKills, err = strconv.Atoi(value)
		case "Time Spent on Fire - Most in Game":
			usr.TimeSpentOnFire, err = parseDuration(value)
		default:

		}
		if err != nil {
			println("Error occured")
			return err
		}
	}
	return nil
}

func (usr *death) Populate(lines []*html.Node) error {
	for _, lineRaw := range lines {
		line := goquery.NewDocumentFromNode(lineRaw)
		text := line.Find("td").First().Text()
		value := strings.Replace(line.Find("td").First().Next().Text(), ",", "", -1)
		var err error
		switch text {
		case "Deaths":
			usr.TotalDeaths, err = strconv.Atoi(value)
		case "Environmental Deaths":
			usr.EnvirronmentalDeaths, err = strconv.Atoi(value)
		default:

		}
		if err != nil {
			println("Error occured")
			return err
		}
	}
	return nil
}

func (usr *average) Populate(lines []*html.Node) error {
	for _, lineRaw := range lines {
		line := goquery.NewDocumentFromNode(lineRaw)
		text := line.Find("td").First().Text()
		value := strings.Replace(line.Find("td").First().Next().Text(), ",", "", -1)
		var err error
		switch text {
		case "Melee Final Blows - Average":
			usr.MeleeFinalBlows, err = strconv.ParseFloat(value, 64)
		case "Time Spent on Fire - Average":
			usr.TimeSpentOnFire, err = parseDuration(value)
		case "Solo Kills - Average":
			usr.SoloKills, err = strconv.ParseFloat(value, 64)
		case "Objective Time - Average":
			usr.ObjectiveTime, err = parseDuration(value)
		case "Objective Kills - Average":
			usr.ObjectiveKills, err = strconv.ParseFloat(value, 64)
		case "Healing Done - Average":
			usr.HealingDone, err = strconv.Atoi(value)
		case "Final Blows - Average":
			usr.TotalFinalBlows, err = strconv.ParseFloat(value, 64)
		case "Deaths - Average":
			usr.Deaths, err = strconv.ParseFloat(value, 64)
		case "All Damage Done - Avg per 10 Min":
			usr.DamageInTenMinutes, err = strconv.Atoi(value)
		case "Eliminations - Average":
			usr.TotalKills, err = strconv.ParseFloat(value, 64)
		default:

		}
		if err != nil {
			println("Error occured")
			return err
		}
	}
	return nil
}

func (usr *awards) Populate(lines []*html.Node) error {
	for _, lineRaw := range lines {
		line := goquery.NewDocumentFromNode(lineRaw)
		text := line.Find("td").First().Text()
		value := strings.Replace(line.Find("td").First().Next().Text(), ",", "", -1)
		var err error
		switch text {
		case "Cards":
			usr.Cards, err = strconv.Atoi(value)
		case "Medals":
			usr.TotalMedals, err = strconv.Atoi(value)
		case "Medals - Bronze":
			usr.BronzeMedals, err = strconv.Atoi(value)
		case "Medals - Silver":
			usr.SilverMedals, err = strconv.Atoi(value)
		case "Medals - Gold":
			usr.GoldMedals, err = strconv.Atoi(value)
		default:

		}
		if err != nil {
			println("Error occured")
			return err
		}
	}
	return nil
}

func (usr *game) Populate(lines []*html.Node) error {
	for _, lineRaw := range lines {
		line := goquery.NewDocumentFromNode(lineRaw)
		text := line.Find("td").First().Text()
		value := strings.Replace(line.Find("td").First().Next().Text(), ",", "", -1)
		var err error
		switch text {
		case "Games Won":
			usr.GamesWon, err = strconv.Atoi(value)
		case "Games Played":
			usr.GamesPlayed, err = strconv.Atoi(value)
		case "Time Spent on Fire":
			usr.TimeSpentOnFire, err = parseDuration(value)
		case "Objective Time":
			usr.ObjectiveTime, err = parseDuration(value)
		case "Time Played":
			usr.HoursPlayed, err = strconv.Atoi(strings.Split(value, " ")[0])
		default:

		}
		if err != nil {
			println("Error occured")
			return err
		}
	}
	return nil
}

func (usr *miscellaneous) Populate(lines []*html.Node) error {
	for _, lineRaw := range lines {
		line := goquery.NewDocumentFromNode(lineRaw)
		text := line.Find("td").First().Text()
		value := strings.Replace(line.Find("td").First().Next().Text(), ",", "", -1)
		var err error
		switch text {
		case "Melee Final Blows - Most in Game":
			usr.MostMeleeFinalBlows, err = strconv.Atoi(value)
		case "Shield Generator Destroyed - Most in Game":
			usr.MostShieldGeneratorsDestroyed, err = strconv.Atoi(value)
		case "Turrets Destroyed - Most in Game":
			usr.MostTurretsDestroyed, err = strconv.Atoi(value)
		case "Environmental Kills - Most in Game":
			usr.MostEnvironnementalKills, err = strconv.Atoi(value)
		case "Teleporter Pad Destroyed - Most in Game":
			usr.MostTelepadsDestroyed, err = strconv.Atoi(value)
		case "Kill Streak - Best":
			usr.BestKillStreak, err = strconv.Atoi(value)
		case "Shield Generators Destroyed":
			usr.TotalShieldGeneratorsDestroyed, err = strconv.Atoi(value)
		case "Turrets Destroyed":
			usr.TotalTurretsDestroyed, err = strconv.Atoi(value)
		case "Recon Assists - Most in Game":
			usr.MostReconAssist, err = strconv.Atoi(value)
		case "Offensive Assists":
			usr.TotalOffensiveAssists, err = strconv.Atoi(value)
		case "Defensive Assists":
			usr.TotalDefensiveAssists, err = strconv.Atoi(value)
		default:

		}
		if err != nil {
			println("Error occured")
			return err
		}
	}
	return nil
}
