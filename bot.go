package main

import (
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func onReady(s *discordgo.Session, event *discordgo.Ready) {
	s.UpdateStatus(0, "!ovstat help")
}

func onNewMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}
	if strings.HasPrefix(m.Content, "!ovstat") {
		c, err := s.State.Channel(m.ChannelID)
		if err != nil {
			// Could not find channel.
			// This shouldn't happen
			return
		}
		args := strings.Split(m.Content, " ")
		lenght := len(args)
		if lenght == 1 {
			s.ChannelMessageSend(c.ID, "The bot it ready! Type ?ovstat help to have more infos")
		} else if lenght == 2 {
			if args[1] == "help" {
				s.ChannelMessageSend(c.ID, "Commands currently available are:\n"+
					"-__?ovstat help__ Shows a list of commands and how to use it\n"+
					"-__?ovstat invite__ Shows my invite link!\n"+
					"-__?ovstat preview #TAG# #us/eu/kr# #pc/xbl/psn#__ Displays one's summary\n"+
					"-__?ovstat competitive/quickplay deaths/combat/assist/awards/misc/games/average/bests #TAG# #us/eu/kr# #pc/xbl/psn#__ Displays a one's stats about the given category")
			}
			if args[1] == "invite" {
				s.ChannelMessageSend(c.ID, "Invite me at https://discordapp.com/oauth2/authorize?client_id=351838420701806592&scope=bot")
			}
		} else if lenght == 5 {
			if args[1] == "preview" {
				usr, err := getUser(args[2], args[4], args[3], true)
				if err != nil {
					s.ChannelMessageSend(c.ID, err.Error())
				} else {
					s.ChannelMessageSend(c.ID, usr.Username+
						" is level **"+strconv.Itoa(usr.Level)+
						"** and their rank is **"+strconv.Itoa(usr.CompetitiveRank)+
						"**. They have killed a total of **"+strconv.Itoa(usr.TotalKills)+
						"** persons and have won a total of **"+strconv.Itoa(usr.TotalGamesWon)+"** games!")
				}
			}
		} else if lenght == 6 {
			switch args[1] {
			case "quickplay":
				usr, err := getUser(args[3], args[5], args[4], true)
				if err != nil {
					s.ChannelMessageSend(c.ID, err.Error())
				}
				switch args[2] {
				case "deaths":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Envirronemental deaths: "+strconv.Itoa(usr.Quickplay.Death.EnvirronmentalDeaths)+"\n"+
						"Total deaths: "+strconv.Itoa(usr.Quickplay.Death.TotalDeaths)+
						"```")
				case "combat":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Envirronemental kills: "+strconv.Itoa(usr.Quickplay.Combat.EnvirronmentalKills)+"\n"+
						"Melee final blows: "+strconv.Itoa(usr.Quickplay.Combat.MeleeFinalBlows)+"\n"+
						"Total final blows: "+strconv.Itoa(usr.Quickplay.Combat.TotalFinalBlows)+"\n"+
						"Multikills: "+strconv.Itoa(usr.Quickplay.Combat.Multikills)+"\n"+
						"Objective kills: "+strconv.Itoa(usr.Quickplay.Combat.ObjectiveKills)+"\n"+
						"Solo kills: "+strconv.Itoa(usr.Quickplay.Combat.SoloKills)+"\n"+
						"Total kills: "+strconv.Itoa(usr.Quickplay.Combat.TotalKills)+"\n"+
						"Total kills: "+strconv.FormatInt(usr.Quickplay.Combat.TotalDamageDone, 10)+"\n"+
						"```")
				case "assist":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Teleport pads destroyed: "+strconv.Itoa(usr.Quickplay.Assist.TelepadsDestroyed)+"\n"+
						"Recon assists: "+strconv.Itoa(usr.Quickplay.Assist.ReconAssist)+" (People you revealed with an ability and that another player killed)\n"+
						"Total healing done: "+strconv.FormatInt(usr.Quickplay.Assist.HealingDone, 10)+"\n"+
						"```")
				case "awards":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Card: "+strconv.Itoa(usr.Quickplay.Awards.Cards)+"\n"+
						"Bronze medals: "+strconv.Itoa(usr.Quickplay.Awards.BronzeMedals)+"\n"+
						"Silver medals: "+strconv.Itoa(usr.Quickplay.Awards.SilverMedals)+"\n"+
						"Gold medals: "+strconv.Itoa(usr.Quickplay.Awards.GoldMedals)+"\n"+
						"Total amount of medals: "+strconv.Itoa(usr.Quickplay.Awards.TotalMedals)+"\n"+
						"```")
				case "bests":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Solo kills: "+strconv.Itoa(usr.Quickplay.Best.SoloKills)+"\n"+
						"Multikills: "+strconv.Itoa(usr.Quickplay.Best.Multikill)+"\n"+
						"Objective kills: "+strconv.Itoa(usr.Quickplay.Best.ObjectiveKills)+"\n"+
						"Final blows: "+strconv.Itoa(usr.Quickplay.Best.FinalBlows)+"\n"+
						"Total kills: "+strconv.Itoa(usr.Quickplay.Best.TotalKills)+"\n"+
						"Offensive assists: "+strconv.Itoa(usr.Quickplay.Best.OffensiveAssists)+"\n"+
						"Defensive assists: "+strconv.Itoa(usr.Quickplay.Best.DefensiveAssists)+"\n"+
						"Healing done: "+strconv.Itoa(usr.Quickplay.Best.HealingDone)+"\n"+
						"Objective time: "+usr.Quickplay.Best.ObjectiveTime.String()+"\n"+
						"Time spent on fire: "+usr.Quickplay.Best.TimeSpentOnFire.String()+"\n"+
						"```")
				case "average":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Solo kills: "+strconv.FormatFloat(usr.Quickplay.Average.SoloKills, 'f', 2, 64)+"\n"+
						"Objective kills: "+strconv.FormatFloat(usr.Quickplay.Average.ObjectiveKills, 'f', 2, 64)+"\n"+
						"Total kills: "+strconv.FormatFloat(usr.Quickplay.Average.TotalKills, 'f', 2, 64)+"\n"+
						"Melee final blows: "+strconv.FormatFloat(usr.Quickplay.Average.MeleeFinalBlows, 'f', 2, 64)+"\n"+
						"Total final blows: "+strconv.FormatFloat(usr.Quickplay.Average.TotalFinalBlows, 'f', 2, 64)+"\n"+
						"Healing done: "+strconv.Itoa(usr.Quickplay.Average.HealingDone)+"\n"+
						"Objective time: "+usr.Quickplay.Average.ObjectiveTime.String()+"\n"+
						"Time spent on fire: "+usr.Quickplay.Average.TimeSpentOnFire.String()+"\n"+
						"Damage done in 10 minutes: "+strconv.Itoa(usr.Quickplay.Average.DamageInTenMinutes)+"\n"+
						"Deaths: "+strconv.FormatFloat(usr.Quickplay.Average.Deaths, 'f', 2, 64)+"\n"+
						"```")
				case "games":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Please be aware that some stats may display 0 because they are not counted in quickplay or competitive\n\n"+
						"Games played: "+strconv.Itoa(usr.Quickplay.Game.GamesPlayed)+"\n"+
						"Games won: "+strconv.Itoa(usr.Quickplay.Game.GamesWon)+"\n"+
						"Hours played: "+strconv.Itoa(usr.Quickplay.Game.HoursPlayed)+"\n"+
						"Objective time: "+usr.Quickplay.Game.ObjectiveTime.String()+"\n"+
						"Time spent on fire: "+usr.Quickplay.Game.TimeSpentOnFire.String()+"\n"+
						"```")
				case "misc":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Best kill streak: "+strconv.Itoa(usr.Quickplay.Miscellaneous.BestKillStreak)+"\n"+
						"Most environnemental kills: "+strconv.Itoa(usr.Quickplay.Miscellaneous.MostEnvironnementalKills)+"\n"+
						"Most melee final blows: "+strconv.Itoa(usr.Quickplay.Miscellaneous.MostMeleeFinalBlows)+"\n"+
						"Most recon assists: "+strconv.Itoa(usr.Quickplay.Miscellaneous.MostReconAssist)+"\n"+
						"Most shield generator destroyed: "+strconv.Itoa(usr.Quickplay.Miscellaneous.MostShieldGeneratorsDestroyed)+"\n"+
						"Most teleport pads destroyed: "+strconv.Itoa(usr.Quickplay.Miscellaneous.MostTelepadsDestroyed)+"\n"+
						"Most turrets destroyed: "+strconv.Itoa(usr.Quickplay.Miscellaneous.MostTurretsDestroyed)+"\n"+
						"Total defensive assists: "+strconv.Itoa(usr.Quickplay.Miscellaneous.TotalDefensiveAssists)+"\n"+
						"Total offensive assists: "+strconv.Itoa(usr.Quickplay.Miscellaneous.TotalOffensiveAssists)+"\n"+
						"Total shield generator destroyed: "+strconv.Itoa(usr.Quickplay.Miscellaneous.TotalShieldGeneratorsDestroyed)+"\n"+
						"Total teleport pads destroyed: "+strconv.Itoa(usr.Quickplay.Miscellaneous.TotalShieldGeneratorsDestroyed)+"\n"+
						"```")
				default:
					s.ChannelMessageSend(c.ID, "Did not find any corresponding field for "+args[2])
				}
			case "competitive":
				usr, err := getUser(args[3], args[5], args[4], true)
				if err != nil {
					s.ChannelMessageSend(c.ID, err.Error())
				}
				switch args[2] {
				case "deaths":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Envirronemental deaths: "+strconv.Itoa(usr.Competitive.Death.EnvirronmentalDeaths)+"\n"+
						"Total deaths: "+strconv.Itoa(usr.Competitive.Death.TotalDeaths)+
						"```")
				case "combat":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Envirronemental kills: "+strconv.Itoa(usr.Competitive.Combat.EnvirronmentalKills)+"\n"+
						"Melee final blows: "+strconv.Itoa(usr.Competitive.Combat.MeleeFinalBlows)+"\n"+
						"Total final blows: "+strconv.Itoa(usr.Competitive.Combat.TotalFinalBlows)+"\n"+
						"Multikills: "+strconv.Itoa(usr.Competitive.Combat.Multikills)+"\n"+
						"Objective kills: "+strconv.Itoa(usr.Competitive.Combat.ObjectiveKills)+"\n"+
						"Solo kills: "+strconv.Itoa(usr.Competitive.Combat.SoloKills)+"\n"+
						"Total kills: "+strconv.Itoa(usr.Competitive.Combat.TotalKills)+"\n"+
						"Total kills: "+strconv.FormatInt(usr.Competitive.Combat.TotalDamageDone, 10)+"\n"+
						"```")
				case "assist":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Teleport pads destroyed: "+strconv.Itoa(usr.Competitive.Assist.TelepadsDestroyed)+"\n"+
						"Recon assists: "+strconv.Itoa(usr.Competitive.Assist.ReconAssist)+" (People you revealed with an ability and that another player killed)\n"+
						"Total healing done: "+strconv.FormatInt(usr.Competitive.Assist.HealingDone, 10)+"\n"+
						"```")
				case "awards":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Card: "+strconv.Itoa(usr.Competitive.Awards.Cards)+"\n"+
						"Bronze medals: "+strconv.Itoa(usr.Competitive.Awards.BronzeMedals)+"\n"+
						"Silver medals: "+strconv.Itoa(usr.Competitive.Awards.SilverMedals)+"\n"+
						"Gold medals: "+strconv.Itoa(usr.Competitive.Awards.GoldMedals)+"\n"+
						"Total amount of medals: "+strconv.Itoa(usr.Competitive.Awards.TotalMedals)+"\n"+
						"```")
				case "bests":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Solo kills: "+strconv.Itoa(usr.Competitive.Best.SoloKills)+"\n"+
						"Multikills: "+strconv.Itoa(usr.Competitive.Best.Multikill)+"\n"+
						"Objective kills: "+strconv.Itoa(usr.Competitive.Best.ObjectiveKills)+"\n"+
						"Final blows: "+strconv.Itoa(usr.Competitive.Best.FinalBlows)+"\n"+
						"Total kills: "+strconv.Itoa(usr.Competitive.Best.TotalKills)+"\n"+
						"Offensive assists: "+strconv.Itoa(usr.Competitive.Best.OffensiveAssists)+"\n"+
						"Defensive assists: "+strconv.Itoa(usr.Competitive.Best.DefensiveAssists)+"\n"+
						"Healing done: "+strconv.Itoa(usr.Competitive.Best.HealingDone)+"\n"+
						"Objective time: "+usr.Competitive.Best.ObjectiveTime.String()+"\n"+
						"Time spent on fire: "+usr.Competitive.Best.TimeSpentOnFire.String()+"\n"+
						"```")
				case "average":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Solo kills: "+strconv.FormatFloat(usr.Competitive.Average.SoloKills, 'f', 2, 64)+"\n"+
						"Objective kills: "+strconv.FormatFloat(usr.Competitive.Average.ObjectiveKills, 'f', 2, 64)+"\n"+
						"Total kills: "+strconv.FormatFloat(usr.Competitive.Average.TotalKills, 'f', 2, 64)+"\n"+
						"Melee final blows: "+strconv.FormatFloat(usr.Competitive.Average.MeleeFinalBlows, 'f', 2, 64)+"\n"+
						"Total final blows: "+strconv.FormatFloat(usr.Competitive.Average.TotalFinalBlows, 'f', 2, 64)+"\n"+
						"Healing done: "+strconv.Itoa(usr.Competitive.Average.HealingDone)+"\n"+
						"Objective time: "+usr.Competitive.Average.ObjectiveTime.String()+"\n"+
						"Time spent on fire: "+usr.Competitive.Average.TimeSpentOnFire.String()+"\n"+
						"Damage done in 10 minutes: "+strconv.Itoa(usr.Competitive.Average.DamageInTenMinutes)+"\n"+
						"Deaths: "+strconv.FormatFloat(usr.Competitive.Average.Deaths, 'f', 2, 64)+"\n"+
						"```")
				case "games":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Please be aware that some stats may display 0 because they are not counted in quickplay or competitive\n\n"+
						"Games played: "+strconv.Itoa(usr.Competitive.Game.GamesPlayed)+"\n"+
						"Games won: "+strconv.Itoa(usr.Competitive.Game.GamesWon)+"\n"+
						"Hours played: "+strconv.Itoa(usr.Competitive.Game.HoursPlayed)+"\n"+
						"Objective time: "+usr.Competitive.Game.ObjectiveTime.String()+"\n"+
						"Time spent on fire: "+usr.Competitive.Game.TimeSpentOnFire.String()+"\n"+
						"```")
				case "misc":
					s.ChannelMessageSend(c.ID, usr.Username+"```Markdown\n"+
						"Best kill streak: "+strconv.Itoa(usr.Competitive.Miscellaneous.BestKillStreak)+"\n"+
						"Most environnemental kills: "+strconv.Itoa(usr.Competitive.Miscellaneous.MostEnvironnementalKills)+"\n"+
						"Most melee final blows: "+strconv.Itoa(usr.Competitive.Miscellaneous.MostMeleeFinalBlows)+"\n"+
						"Most recon assists: "+strconv.Itoa(usr.Competitive.Miscellaneous.MostReconAssist)+"\n"+
						"Most shield generator destroyed: "+strconv.Itoa(usr.Competitive.Miscellaneous.MostShieldGeneratorsDestroyed)+"\n"+
						"Most teleport pads destroyed: "+strconv.Itoa(usr.Competitive.Miscellaneous.MostTelepadsDestroyed)+"\n"+
						"Most turrets destroyed: "+strconv.Itoa(usr.Competitive.Miscellaneous.MostTurretsDestroyed)+"\n"+
						"Total defensive assists: "+strconv.Itoa(usr.Competitive.Miscellaneous.TotalDefensiveAssists)+"\n"+
						"Total offensive assists: "+strconv.Itoa(usr.Competitive.Miscellaneous.TotalOffensiveAssists)+"\n"+
						"Total shield generator destroyed: "+strconv.Itoa(usr.Competitive.Miscellaneous.TotalShieldGeneratorsDestroyed)+"\n"+
						"Total teleport pads destroyed: "+strconv.Itoa(usr.Competitive.Miscellaneous.TotalShieldGeneratorsDestroyed)+"\n"+
						"```")
				default:
					s.ChannelMessageSend(c.ID, "Did not find any corresponding stat field for "+args[2])
				}
			default:
				s.ChannelMessageSend(c.ID, "Did not find any corresponding gamemode field for "+args[2])
			}
		}
	}
}
